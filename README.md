<p align="center">
    <img src="https://github.com/mjpitz/mjpitz/raw/main/banner-2.png"/>
</p>

<h3 align="center">👋 Welcome to my profile!</h3>

<p align="center">
    <a href="https://blacklivesmatter.com/">
        <img alt="Black Lives Matter" src="https://img.shields.io/badge/-Black%20Lives%20Matter-gray?style=for-the-badge" /></a>
    <a href="https://www.thetrevorproject.org/">
        <img alt="Diversity Makes us Stronger" src="https://img.shields.io/badge/-Diversity%20Makes%20Us%20Stronger-pink?style=for-the-badge&labelColor=silver" /></a>
</p>

<h3 align="center">About Me</h3>

<p align="center">
    My name is Mya.
    I'm a software engineer who enjoys working with distributed systems.
    Outside of work, I'm passionate about doing more to save our environment ♻️.
    While much of my learning has been confined to my own space, I look forward to sharing my knowledge with others.
</p>

<p align="center">
    <a href="https://www.google.com/search?q=pronunciation+maya">
        <img alt="Name Pronunciation" src="https://img.shields.io/badge/Pronunciation%20%F0%9F%94%88-mai·uh-silver?style=for-the-badge&labelColor=silver" /></a>
    <a href="https://pronoun.is/she">
        <img alt="Pronouns: she / her" src="https://img.shields.io/badge/Pronouns-she%2Fher-pink?style=for-the-badge&labelColor=silver" /></a>
</p>

<p align="center">
    <a href="https://mjpitz.com">
        <img alt="Blog" src="https://img.shields.io/badge/-Blog-silver?style=for-the-badge" /></a>
    <a href="https://github.com/mjpitz/mjpitz/tree/main/papers">
        <img alt="Papers" src="https://img.shields.io/badge/-Papers-silver?style=for-the-badge" /></a>
    <a href="https://github.com/mjpitz/mjpitz/tree/main/talks">
        <img alt="Presentations" src="https://img.shields.io/badge/-Presentations-pink?style=for-the-badge" /></a>
    <a href="https://github.com/mjpitz/mjpitz/tree/main/homestead">
        <img alt="Homestead" src="https://img.shields.io/badge/-Homestead-282?style=for-the-badge" /></a>
</p>

<h3 align="center">My Projects</h3>

<p align="center">
    This is by no means a complete collection of my work, rather a select few that I'm excited about.
</p>

<p align="center">
    <a href="https://deps.cloud">
        <img alt="deps.cloud" src="https://img.shields.io/badge/-deps.cloud-white?style=for-the-badge&logo=data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAACMElEQVRYR+2WPUgbYRjH/5fLhzHpaTDNkFpxuFKTIgg6KSXSoZDQoegQ2ioWuqVToaDZ+uFSOhUFQTNkiUnp0A5Nz6VjS6BFmsEqpaJBm6RJKhc5Iya5XLkOoeHuktMlCPeu7/Pxe//PBy8x/npbQBsPMRrVADQFNAU0Bc6pAtd6TPBf7UIfZUS+VMW7n4f4lC6deqeeaROOX7bg6agDJEE0JFxKHiCyyZ4KQhXABaMOEzQFp9WArYMTPBi0odtEShLxNQH++B6yR1XVEC0BrEYdQjcvoddqUBX05dcCPqY4cJWaKvuWAHcGuvBwqKdpsEhkFT6fFzabDYIAiJX5xVXw4ksB67+Pm/q2BHg8YsdtmlIMEovFMDs7h1RqV2JzXK3h3od95ErKJVEEuGjW49mYA4P2DkngRCIBhlkDTdOIRqNIJpPweDz/Xh4KhWA2m+s+r9YLePPjUPEBigCLN5wYckiTi5EEQUAwGMTy8ko9MEVRYBgGLtdAQ7KdYhlPPuewXSzLQsgCiF3PTPTLOmSzWWxsfEc8Hkc4HK7bBAIBzM8/l/VJcxX43+9B7uslC2AiCaxN9sOga5zzTCaDmZn76LR0wuf1gWVZ5PN5uN1uTE9PQa/XK0p9620K7AkvuVcswaNhOyavSJuP53mQpHQHNGv13WIZU8y++hKIliQB3HV1w9NrgaiI2vO/zOJIivIvfPuDNCc/CS3HUG3is9ppAJoCmgKaAsT1WHt/xX8BCJf2zZY05lEAAAAASUVORK5CYII=" /></a>
    <a href="https://aetherfs.tech">
        <img alt="AetherFS" src="https://img.shields.io/badge/🔐-Aether%20FS-white?style=for-the-badge&labelColor=white" /></a>
    <a href="https://github.com/mjpitz/mjpitz/tree/main/projects/graphstore.md">
        <img alt="GraphStore" src="https://img.shields.io/badge/🔐-Graph%20Store-white?style=for-the-badge&labelColor=white" /></a>
</p>

<h3 align="center">Contact / Connect</h3>

<p align="center">
    <i>Don't hesitate to contact me!</i>
    I'm always happy to chat 😊
</p>

<p align="center">
    <a href="https://twitter.com/SoSoCoder">
        <img alt="Twitter" src="https://img.shields.io/badge/-Twitter-gray?style=for-the-badge&logo=twitter&logoColor=white" /></a>
    <a href="https://linkedin.com/in/mjpitz">
        <img alt="LinkedIn" src="https://img.shields.io/badge/-Linked%20In-gray?style=for-the-badge&logo=linkedin" /></a>
    <a href="https://forms.gle/uCMy38ZLEchfNuka9">
        <img alt="Project Interest Form" src="https://img.shields.io/badge/-Project%20Interest%20Form-blue?style=for-the-badge" /></a>
</p>

![](https://www.google-analytics.com/collect?v=1&tid=UA-172921913-1&cid=555&t=pageview&ec=repo&ea=open&dp=%2F&dt=%2F)
