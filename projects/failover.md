# Failover

[![Project Interest Form](https://img.shields.io/badge/-Project%20Interest%20Form-blue?style=for-the-badge)](https://forms.gle/uCMy38ZLEchfNuka9)

Failover is a generalized container solution that manages coordinated failovers amongst a set of cluster peers. This is
an extension of my previous redis-vip solution that I assembled as an initial proof of concept.

## Status

- Documentation
  - [ ] Design Document
- Features
  - [ ] Core functionality
  - [ ] 
