# Projects

## Open Source

[![deps.cloud][]](https://deps.cloud)

[deps.cloud]: https://img.shields.io/badge/-deps.cloud-white?style=for-the-badge&logo=data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAACMElEQVRYR+2WPUgbYRjH/5fLhzHpaTDNkFpxuFKTIgg6KSXSoZDQoegQ2ioWuqVToaDZ+uFSOhUFQTNkiUnp0A5Nz6VjS6BFmsEqpaJBm6RJKhc5Iya5XLkOoeHuktMlCPeu7/Pxe//PBy8x/npbQBsPMRrVADQFNAU0Bc6pAtd6TPBf7UIfZUS+VMW7n4f4lC6deqeeaROOX7bg6agDJEE0JFxKHiCyyZ4KQhXABaMOEzQFp9WArYMTPBi0odtEShLxNQH++B6yR1XVEC0BrEYdQjcvoddqUBX05dcCPqY4cJWaKvuWAHcGuvBwqKdpsEhkFT6fFzabDYIAiJX5xVXw4ksB67+Pm/q2BHg8YsdtmlIMEovFMDs7h1RqV2JzXK3h3od95ErKJVEEuGjW49mYA4P2DkngRCIBhlkDTdOIRqNIJpPweDz/Xh4KhWA2m+s+r9YLePPjUPEBigCLN5wYckiTi5EEQUAwGMTy8ko9MEVRYBgGLtdAQ7KdYhlPPuewXSzLQsgCiF3PTPTLOmSzWWxsfEc8Hkc4HK7bBAIBzM8/l/VJcxX43+9B7uslC2AiCaxN9sOga5zzTCaDmZn76LR0wuf1gWVZ5PN5uN1uTE9PQa/XK0p9620K7AkvuVcswaNhOyavSJuP53mQpHQHNGv13WIZU8y++hKIliQB3HV1w9NrgaiI2vO/zOJIivIvfPuDNCc/CS3HUG3is9ppAJoCmgKaAsT1WHt/xX8BCJf2zZY05lEAAAAASUVORK5CYII=

## Closed Source

<a href="aetherfs.md"><img alt="AetherFS" src="https://img.shields.io/badge/🔐-Aether%20FS-white?style=for-the-badge&labelColor=white"/></a>

<a href="failover.md"><img alt="GraphStore" src="https://img.shields.io/badge/🔐-Failover-white?style=for-the-badge&labelColor=white"/></a>

<a href="graphstore.md"><img alt="GraphStore" src="https://img.shields.io/badge/🔐-Graph%20Store-white?style=for-the-badge&labelColor=white"/></a>
