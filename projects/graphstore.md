# GraphStore

[![Project Interest Form](https://img.shields.io/badge/-Project%20Interest%20Form-blue?style=for-the-badge)](https://forms.gle/uCMy38ZLEchfNuka9)

GraphStore is a lightweight, key-value graph database.

**Why?**

Mostly an exercise currently, but I also want to explore some other interesting possibilities. 

**Status**

This is mostly in the early stages of development.
There isn't much code currently.
Still working on design.

- Documentation
  - [ ] Design Document
- Features
  - [ ] TBD
